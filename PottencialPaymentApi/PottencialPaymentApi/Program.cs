using Microsoft.EntityFrameworkCore;
using PottencialPaymentApi.Data;
using PottencialPaymentApi.Data.Repository;
using PottencialPaymentApi.Interfaces;
using PottencialPaymentApi.Mappers;
using PottencialPaymentApi.Service;

var builder = WebApplication.CreateBuilder(args);
ConfigureServices(builder);

// Add services to the container.
builder.Services.AddMvc(options =>
{
    options.SuppressAsyncSuffixInActionNames = true;
});

builder.Services.AddAutoMapper(typeof(AutoMapperConfig));

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI(options =>
    {
        options.RoutePrefix = "api-docs";
        options.SwaggerEndpoint("/swagger/v1/swagger.json", "Pottencial API");
    });
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();

void ConfigureServices(WebApplicationBuilder builder)
{
    var connectionString = builder.Configuration.GetConnectionString("DefaultConnection");
    builder.Services.AddDbContext<DataContext>(options =>
    {
        options.UseSqlServer(connectionString);
    });

    builder.Services.AddTransient<IVendaService, VendaService>();
    builder.Services.AddTransient<IVendaRepository, VendaRepository>();
}