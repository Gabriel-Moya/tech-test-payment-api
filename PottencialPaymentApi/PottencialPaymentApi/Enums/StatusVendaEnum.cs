﻿namespace PottencialPaymentApi.Enums
{
    public enum StatusVendaEnum
    {
        AGUARDANDO_PAGAMENTO,
        PAGAMENTO_APROVADO,
        ENVIADO_PARA_TRANSPORTADORA,
        ENTREGUE,
        CANCELADA
    }
}
