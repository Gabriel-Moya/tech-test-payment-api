﻿using PottencialPaymentApi.Enums;

namespace PottencialPaymentApi.Models;

public class Venda : Base
{
    public Venda() { }

    public Venda(DateTime dataVenda, Vendedor vendedor, StatusVendaEnum status, List<ItemVenda> itens)
    {
        DataVenda = dataVenda;
        Vendedor = vendedor;
        Status = status;
        Itens = itens;
    }

    public DateTime DataVenda { get; set; }
    public Guid VendedorId { get; set; }
    public Vendedor Vendedor { get; set; }
    public StatusVendaEnum Status { get; set; }
    public List<ItemVenda> Itens { get; set; }
}
