﻿namespace PottencialPaymentApi.Models;

public class Vendedor : Base
{
	public Vendedor(string cpf, string nome, string email, string telefone)
    {
        Cpf = cpf;
        Nome = nome;
        Email = email;
        Telefone = telefone;
    }

    public string Cpf { get; private set; }
    public string Nome { get; private set; }
    public string Email { get; private set; }
    public string Telefone { get; private set; }

    public IList<Venda> Vendas { get; set; }
}
