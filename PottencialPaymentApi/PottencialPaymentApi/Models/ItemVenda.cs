﻿namespace PottencialPaymentApi.Models;

public class ItemVenda : Base
{
    public ItemVenda(string descricao, decimal preco)
    {
        Descricao = descricao;
        Preco = preco;
    }

    public string Descricao { get; set; }
    public decimal Preco { get; set; }

    public IList<Venda> Vendas { get; set; }
}
