﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PottencialPaymentApi.Models;

namespace PottencialPaymentApi.Data.Mappings;

public class ItemVendaMap : IEntityTypeConfiguration<ItemVenda>
{
    public void Configure(EntityTypeBuilder<ItemVenda> builder)
    {
        // Tabela
        builder.ToTable("ItemVenda");

        // Chave primária
        builder.HasKey(x => x.Id);

        // Propriedades
        builder.Property(x => x.Descricao).IsRequired();
        builder.Property(x => x.Preco).IsRequired();
    }
}
