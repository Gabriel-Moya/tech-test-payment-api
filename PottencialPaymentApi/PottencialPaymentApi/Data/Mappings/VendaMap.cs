﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PottencialPaymentApi.Models;

namespace PottencialPaymentApi.Data.Mappings;

public class VendaMap : IEntityTypeConfiguration<Venda>
{
    public void Configure(EntityTypeBuilder<Venda> builder)
    {
        // Tabela
        builder.ToTable("Venda");

        // Chave primária
        builder.HasKey(x => x.Id);

        // Propriedades
        builder.Property(x => x.DataVenda).IsRequired();
        builder.Property(x => x.Status).IsRequired();

        // Relacionamentos
        builder.HasOne(x => x.Vendedor)
            .WithMany(x => x.Vendas)
            .HasConstraintName("FK_Venda_Vendedor");

        builder.HasMany(x => x.Itens)
            .WithMany(x => x.Vendas)
            .UsingEntity<Dictionary<string, object>>(
                "VendaItem",
                venda => venda.HasOne<ItemVenda>()
                    .WithMany()
                    .HasForeignKey("VendaId")
                    .HasConstraintName("FK_Venda_Item_VendaId"),
                item => item.HasOne<Venda>()
                    .WithMany()
                    .HasForeignKey("ItemVendaId")
                    .HasConstraintName("FK_VendaItem_ItemId")
        );
    }
}
