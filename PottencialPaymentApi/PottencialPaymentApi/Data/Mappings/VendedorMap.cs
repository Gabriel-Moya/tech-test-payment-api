﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PottencialPaymentApi.Models;

namespace PottencialPaymentApi.Data.Mappings;

public class VendedorMap : IEntityTypeConfiguration<Vendedor>
{
    public void Configure(EntityTypeBuilder<Vendedor> builder)
    {
        // Tabela
        builder.ToTable("Vendedor");

        // Chave primária
        builder.HasKey(x => x.Id);

        // Propriedades
        builder.Property(x => x.Cpf)
            .IsRequired()
            .HasColumnType("NVARCHAR")
            .HasMaxLength(11);

        builder.Property(x => x.Nome).IsRequired();
        builder.Property(x => x.Email).IsRequired();

        builder.Property(x => x.Telefone)
            .IsRequired()
            .HasColumnType("NVARCHAR")
            .HasMaxLength(11);
    }
}
