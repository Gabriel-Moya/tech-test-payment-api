﻿using Microsoft.EntityFrameworkCore;
using PottencialPaymentApi.Data.Mappings;
using PottencialPaymentApi.Models;

namespace PottencialPaymentApi.Data;

public class DataContext : DbContext
{
    public DataContext(DbContextOptions<DataContext> options) : base(options) { }

	public DbSet<Venda> Venda { get; set; }
	public DbSet<Vendedor> Vendedor { get; set; }
	public DbSet<ItemVenda> ItemVenda { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
	{
        modelBuilder.ApplyConfiguration(new VendaMap());
        modelBuilder.ApplyConfiguration(new VendedorMap());
        modelBuilder.ApplyConfiguration(new ItemVendaMap());
    }
}
